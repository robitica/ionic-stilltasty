export interface ShoppingList {
    key?: string;
    image: string;
    name: string;
}