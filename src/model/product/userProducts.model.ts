export interface userProducts {
    key?: string;
    title: string;
    image: string;
    content: string;
}