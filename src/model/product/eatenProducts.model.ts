export interface EatenProductsList {
    key?: string;
    image: string;
    name: string;
}